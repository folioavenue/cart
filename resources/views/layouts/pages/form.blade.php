@extends('layouts.template.master')

@section('custom_css')

@endsection

@section('content')
     <!-- Start Content-->
     <div class="container-fluid">
                        
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Hyper</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Forms</a></li>
                                <li class="breadcrumb-item active">Form Elements</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Form Elements</h4>
                    </div>
                </div>
            </div>     
            <!-- end page title --> 

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Testing Form</h4>
                          
                            <div class="row">
                                <div class="col-lg-6">
                                <form action="{{ route('cart.store') }}" method="POST" role="form">
                                        @csrf
                                        <div class="form-group mb-3">
                                            <label for="simpleinput">Book Title</label>
                                            <input type="text" id="simpleinput" class="form-control" name="book_title" placeholder="Book Title">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="example-email">Price</label>
                                            <input type="integer" id="example-email" name="book_price" class="form-control" placeholder="Price">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="example-password">Secret key</label>
                                            <input type="password" id="example-password" name="secret_key" class="form-control" placeholder="Secret Key">
                                        </div>
                                        <div class="form-group mb-3">
                                            <button type="submit" class="btn btn-success">Order</button>
                                        </div>                                        
                                    </form>
                                </div> <!-- end col -->                               
                            </div>
                            <!-- end row-->

                        </div> <!-- end card-body -->
                    </div> <!-- end card -->
                </div><!-- end col -->
            </div>
            <!-- end row -->       
            
        </div> <!-- container -->
@endsection

@section('custom_js')
    
   

@endsection
