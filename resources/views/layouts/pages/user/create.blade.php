@extends('layouts.template.master')

@section('custom_css')

@endsection

@section('content')
     <!-- Start Content-->
     <div class="container-fluid">
                        
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Folioavenue</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Forms</a></li>
                                <li class="breadcrumb-item active">Add New User Author</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Add New User Author</h4>
                    </div>
                </div>
            </div>     
            <!-- end page title --> 

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('user.store') }}" method="POST" role="form">
                                    @csrf
                          
                                <div class="row">
                                    <div class="col-lg-6">
                                            <h4 class="header-title">Author Information</h4>

                                            <div class="form-group mb-3">
                                                <label for="name">Name</label>
                                                <input type="text" id="name" class="form-control" name="name" placeholder="name" required>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="email">Email</label>
                                                <input type="email" id="email" class="form-control" name="email" placeholder="Email" required>
                                            </div>
                                            
                                            <div class="form-group mb-3">
                                                <label for="website">Website</label>
                                                <input type="text" id="website" class="form-control" name="website" placeholder="http://" required>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="api_client_id">Api CLient ID</label>
                                                <input type="password" id="api_client_id" class="form-control" name="api_client_id" placeholder="Api CLient ID" required>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="api_secret_key">Api Secret Key</label>
                                                <input type="password" id="api_secret_key" name="api_secret_key" class="form-control" placeholder="Api Secret Key" required>
                                            </div>  
                                    
                                    </div> <!-- end col -->
                                    <div class="col-lg-6">
                                            <h4 class="header-title">Credentials</h4>
                                            <div class="form-group mb-3">
                                                <label for="username">Username</label>
                                                <input type="text" id="username" class="form-control" name="username" placeholder="Username" required>
                                            </div>

                                            <div class="form-group mb-3">
                                                    <label for="password">Password</label>
                                                    <input type="password" id="password" class="form-control" name="password" placeholder="Password" required>
                                                </div>
                                                <div class="form-group mb-3">
                                                        <label for="cpassword">Confirm Password</label>
                                                        <input type="password" id="cpassword" class="form-control" name="cpassword" placeholder="Confirm Password" required>
                                                </div>
                                    </div>     
                                     <button type="submit" class="btn btn-success btn-block">Save</button>                                                    
                                </div>
                                <!-- end row-->
                             </form>
                        </div> <!-- end card-body -->
                    </div> <!-- end card -->
                </div><!-- end col -->
            </div>
            <!-- end row -->       
            
        </div> <!-- container -->
@endsection

@section('custom_js')
    
   

@endsection
