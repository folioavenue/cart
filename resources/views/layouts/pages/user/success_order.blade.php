@extends('layouts.template.master_cart')

@section('content')
<div class="content-page">
        <div class="content">
            
            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
               
                <!-- end page title -->
            <form action="{{ url('/cart/check-out-with-paypal') }}" role="form" method="POST">
                    @csrf
                <div class="row" style="margin-top:10%;">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div id="empty-cart" style="text-align:center;">
                                        <span><i class=" mdi mdi-approval text-success" style="font-size:100px;"></i></span>
                                        <h1 class="text-success">Your order has been succesfully processed!</h1>
                                    <a href="#" class="btn btn-success"><i class="mdi mdi-arrow-left"></i> go back to book store</a>
                                    </div>
                                    <br>
                                    <br>
                                    
                                </div> <!-- end card body-->
                                
                            </div> <!-- end card -->
                          
                        </div><!-- end col-->

                                     
                    </div>
                </form>
                    <!-- end row-->
                
            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div style="text-align:center">
                            2018 © <a href="folioavenue.com" style="color:#0acf97">Folioavenue</a> 
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>
@endsection