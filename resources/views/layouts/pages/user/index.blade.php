@extends('layouts.template.master')

@section('custom_css')
    <link href="{{ asset('vendor/hyper/assets/css/vendor/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    
    <link href="{{ asset('vendor/hyper/assets/css/vendor/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
     <!-- Start Content-->
     <div class="container-fluid">
                        
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Folioavenue</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Author Lists</a></li>
                           
                            </ol>
                        </div>
                        <h4 class="page-title">Author Lists</h4>
                    </div>
                </div>
            </div>     
            <!-- end page title --> 

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a class="btn btn-success btn-block" href="{{ url('/user/create') }}">Add New Author</a>
                            <br>       
                            <table id="authorstable" class="table dt-responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Access key</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($authors as $author)
                                                <tr>
                                                    <td>{{ ucfirst($author->name) }}</td>
                                                    <td>{{ $author->email }}</td>
                                                    <td><a href="{{ $author->website }}" target="_blank">{{ $author->website}}</a></td>
                                                    <td>{{ $author->access_key }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    
                                    </table>                       
                        </div> <!-- end card-body -->
                    </div> <!-- end card -->
                </div><!-- end col -->
            </div>
            <!-- end row -->                   
        </div> <!-- container -->
@endsection

@section('custom_js')    
    <script src="{{ asset('vendor/hyper/assets/js/vendor/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendor/hyper/assets/js/vendor/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('vendor/hyper/assets/js/vendor/dataTables.responsive.min.js') }}"></script>

    <script>
        $(document).ready(function(){                         
                $("#authorstable").DataTable();
        });
    </script>

@endsection
