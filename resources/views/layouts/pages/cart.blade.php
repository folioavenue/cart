@extends('layouts.template.master_cart')

@section('custom_css')
    <style>
        #loader{
            display:none;
        }
    </style>
@endsection

@section('content')
<div class="content-page">
        <div class="content">
            
            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            
                        <h4 class="page-title" style="text-align:center;">Welcome to {{ $book_store }}</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->
            <form action="{{ url('/cart/check-out-with-paypal') }}" role="form" method="POST">
                    @csrf
                <div class="row">
                        <div class="col-xl-9">
                            <div class="card">
                                <div class="card-body">
                                    <div id="empty-cart" style="text-align:center;">
                                        <span><i class="mdi mdi-cart-off" style="font-size:100px;"></i></span>
                                        <h1>Your cart is Empty</h1>
                                    <a href="{{ $link }}" class="btn btn-success"><i class="mdi mdi-arrow-left"></i> go back to book store</a>
                                    </div>
                                    <div id="cart">
                                        <h4 class="header-title"><i class="mdi mdi-cart"></i> Shopping Cart</h4>
                                        {{-- <p class="text-muted font-14 mb-4">
                                            Add <code>.table-hover</code> to enable a hover state on table rows within a <code>&lt;tbody&gt;</code>.
                                        </p> --}}
                                        <br>
                                        <div class="table-responsive-sm">
                                            <table class="table table-hover table-centered mb-0" >
                                                <thead>
                                                    <tr>
                                                        <th style="width:40%">Product</th>
                                                        <th style="width:20%">Price</th>
                                                        <th style="width:20%">Quantity</th>
                                                        <th style="width:15%">Amount</th>
                                                        <th style="width:5%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $total_quantity = [];
                                                        $key = 0;
                                                    @endphp
                                                    @foreach($items->unique('book_title') as $item)
                                                    <tr id="tb{{ $item['book_id'] }}">
                                                        <td>{{ $item['book_title'] }}</td>
                                                        <td>{{ number_format($item['book_price'],2) }}</td>
                                                        <td>
                                                                <input type="hidden" name="key" id="key{{ $item['book_id'] }}" value="{{ $key }}">   
                                                            <input type="hidden" id="temp_quantity{{ $item['book_id'] }}" value="1">
                                                            <select name="quantity[]" id="quantity{{ $item['book_id'] }}" class="form-control" onChange="update({{ $item['book_id'] }})">
                                                                @for($i = 1;$i<=50;$i++)
                                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                                @endfor                                                            
                                                            </select>     
                                                            <input type="hidden" name="book_id[]" id="book_id{{ $item['book_id'] }}" value="{{ $item['book_id'] }}">                                                   
                                                            <input type="hidden" name="book_title[]" value="{{ $item['book_title'] }}">
                                                            <input type="hidden" name="book_price[]" id="book_price{{ $item['book_id'] }}" value="{{ number_format($item['book_price'],2) }}">
                                                        
                                                        </td>
                                                        <td id="t_book_price{{ $item['book_id'] }}">${{ number_format($item['book_price'],2) }}</td>
                                                        <td><span onClick="del({{ $item['book_id'] }});"><i class="mdi mdi-delete mdi-2x text-danger" style="font-size:20px;cursor:pointer"></i></span></td>
                                                    </tr>
                                                        @php
                                                            $total_quantity[] =  1;
                                                            $key++;
                                                        @endphp
                                                    @endforeach
                                                    <input type="hidden" name="shipping" id="shipping" value="{{ number_format($item['shipping'],2) }}">
                                                    <input type="hidden" name="handling" id="handling" value="{{ number_format($item['handling'],2) }}">
                                                    <input type="hidden" id="total_quantity" value="{{ array_sum($total_quantity) }}">
                                                </tbody>
                                            </table>
                                        </div> <!-- end table-responsive-->
                                     </div>
                                    
                                </div> <!-- end card body-->
                                
                            </div> <!-- end card -->
                            <p class="text-muted font-13" style="text-align:center;">
                                <i class="mdi mdi-information text-success"></i> Note, Maximum of 50 order per transaction.                             </p>
                        </div><!-- end col-->

                        <div class="col-xl-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div id="loader" style="margin-top:40%;margin-left:5%;margin-right:5%;">
                                            <img src="{{ asset('images/loader.gif') }}" alt="" width="100%" height="100%">
                                        </div>
                                      
                                        <div id="details">
                                            <h4 class="header-title"> Total </h4>                                       
                                            <br>
                                            <div class="chart-widget-list">
                                                <p class="mb-0">
                                                        <i class="mdi mdi-library-books text-success"></i> Book Price
                                                        <input type="hidden" id="init_book_price" value="{{ $total_price }}">
                                                        <span class="float-right" id="total_book_price">${{ number_format($total_price,2) }}</span>
                                                    </p>
                                                <p>
                                                    <i class="mdi mdi-truck-fast text-primary"></i> Shipping
                                                    <input type="hidden" id="total_shipping_price" value="{{ $total_shipping }}">
                                                    <span class="float-right" id="total_shipping_price_display">${{ number_format($total_shipping,2) }}</span>
                                                </p>
                                                <p>
                                                    <i class="mdi mdi-hand-pointing-right text-danger"></i> Handling
                                                <span class="float-right" id="total_handling_price">${{ number_format($handling,2)   }}</span>
                                                </p>
                                                <p>
                                                    <i class="mdi mdi-cash text-default"></i> Tax
                                                    <span class="float-right">May Vary</span>
                                                </p>
                                                <br>
                                                <br>
                                                <p>
                                                    Total 
                                                <span class="float-right" id="total_price">${{ number_format(($total_price+$total_shipping+$handling),2) }}</span>
                                                </p>
                                                <br>
                                                <br>
                                                <button type="submit" class="btn btn-rounded btn-warning"><strong style="color:white;">Check Out with <i class="mdi mdi-paypal text-primary"></i> Paypal</strong></span></button>
                                            </div>
                                        </div>
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col-->                      
                    </div>
                </form>
                    <!-- end row-->
                
            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div style="text-align:center">
                            2018 © <a href="folioavenue.com" style="color:green">Folioavenue</a> 
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>
@endsection

@section('custom_js')
    <script src="{{ asset('vendor/number/jquery.number.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#empty-cart').hide();
            $('#loader').hide();
        });

            function update(book_id){
                
                   $("#details").hide();  
                   $("#loader").show(500); 
                   var book_price = $("#book_price"+book_id).val();
                   var shipping   = $("#shipping").val();
                   var handling   = $("#handling").val();
                   var quantity   = $("#quantity"+book_id).val();
                   var temp_quantity = $("#temp_quantity"+book_id).val();
                   var init_shipping_price  = $("#total_shipping_price").val(); 
                   var total_quantity = $("#total_quantity").val();
                   var init_book_price = $("#init_book_price").val();      
                   var new_quantity = parseInt(total_quantity);
                   var total_shipping_price = 0;
                   var total_init_book_price = 0;
                   var additional_shipping = 0.55;
                   var prev_book_price = 0;
                   var prev_shipping_price = 0;
                   
                 //   if(temp_quantity != quantity){     
                 //   if(quantity > temp_quantity){ 
                    prev_book_price       =   parseFloat(init_book_price) - (parseFloat(book_price) * parseInt(temp_quantity));
                    total_init_book_price =   (parseFloat(book_price) * parseInt(quantity)) + parseFloat(prev_book_price);
                    prev_shipping_price   =   parseFloat(init_shipping_price) - (parseFloat(additional_shipping) * parseInt(temp_quantity))
                    total_shipping_price  =   parseFloat(prev_shipping_price) + (parseFloat(additional_shipping) * parseInt(quantity));
                 //   }else if(quantity < temp_quantity){
                 //      total_init_book_price =   parseFloat(init_book_price) - parseFloat(book_price);
                        //total_shipping_price =    parseFloat(init_shipping_price) - parseFloat(additional_shipping);
                 //   }                               
                    
                     var total_price = parseInt(total_init_book_price)  + total_shipping_price + parseFloat(handling);
                     $("#total_book_price").html("$"+$.number(total_init_book_price,2));
                     $("#t_book_price"+book_id).html("$"+$.number(book_price*quantity,2));
                     $("#total_shipping_price_display").html("$"+$.number(total_shipping_price,2));  
                     $("#total_shipping_price").val(total_shipping_price);
                     $("#total_price").html("$"+$.number(total_price,2));
                     $("#init_book_price").val(total_init_book_price);
                     $("#temp_quantity"+book_id).val(quantity);
                     $("#total_quantity").val(total_quantity);                    
                     $("#loader").hide(); 
                     $("#details").show(1300);   

                //   }
               
        }

        function del(book_id){
                   $("#details").hide();  
                   $("#loader").show(500);  
                   var id         = $("#key"+book_id).val();
                   var book_price = $("#book_price"+book_id).val();
                   var shipping   = $("#shipping").val();
                   var handling   = $("#handling").val();
                   var quantity   = $("#quantity"+book_id).val();
                   var temp_quantity = $("#temp_quantity"+book_id).val();
                   var init_shipping_price  = $("#total_shipping_price").val(); 
                   var total_quantity = $("#total_quantity").val();
                   var init_book_price = $("#init_book_price").val();      
                   var new_quantity = parseInt(total_quantity - quantity);
                   var total_shipping_price = 0;
                   var total_init_book_price = 0;
                   var additional_shipping = 0.55;

                  total_init_book_price =   parseFloat(init_book_price) - (parseFloat(book_price) * parseInt(quantity));
                  total_shipping_price =    parseFloat(init_shipping_price) - (parseFloat(additional_shipping) * parseInt(quantity));

                   var total_price = parseInt(total_init_book_price)  + total_shipping_price + parseFloat(handling);

                    $("#total_book_price").html("$"+$.number(total_init_book_price,2));
                    $("#total_shipping_price_display").html("$"+$.number(total_shipping_price,2));  
                    $("#total_shipping_price").val(total_shipping_price);
                    $("#total_price").html("$"+$.number(total_price,2));
                    $("#init_book_price").val(total_init_book_price);
                    $("#total_quantity").val(new_quantity);
   
                    $("#tb"+book_id).remove();

                    if(new_quantity == 0){
                        $("#cart").hide();
                        $("#loader").hide(); 
                        $("#empty-cart").show(100);
                    }else{
                        $("#loader").hide(); 
                        $("#details").show(1300);   
                    }

                       
                    $.ajax({
                        url:"{{ URL::to('/cart/delete-item') }}",
                        method:'POST',
                        data:{id:id,new_quantity:new_quantity},
                        success:function(data){        
                            console.log(data);        
                            console.log('successfully deleted item!');
                        },
                        error:function(data){
                            console.log('fail delete item!');
                            console.log(data);
                        }
                    });            
                    
                    console.log('newquantity'+new_quantity);      
        }

    </script>
@endsection
