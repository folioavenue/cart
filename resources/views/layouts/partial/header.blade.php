<!DOCTYPE html>
    <html lang="en">

    
<!-- Mirrored from coderthemes.com/hyper/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Oct 2018 18:52:38 GMT -->
<head>
        <meta charset="utf-8" />
        <title>FolioAvenue Cart -  Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('images/cart.ico') }}">

        <!-- third party css -->
        <link href="{{ asset('vendor/hyper/assets/css/vendor/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
        
        
        <!-- App css -->
        <link href="{{ asset('vendor/hyper/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('vendor/hyper/assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

        

        @yield('custom_css')

    </head>