<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
          'user_type'=> 1,
          'username' => 'folioavenue',
          'name'     => 'FolioAvenue Admin',
          'email'    => 'folioavenue.rickymillang@gmail.com',
          'api_client_id'  => null,
          'api_secret_key' => null,
          'access_key'     => null,
          'password'       => bcrypt('nopassword123'),
      ]);
    }
}
