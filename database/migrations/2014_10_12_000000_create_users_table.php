<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_type')->unsigned();
            $table->string('username');
            $table->string('name');
            $table->string('email')->index();
            $table->text('website');
            $table->text('api_client_id')->nullable();
            $table->text('api_secret_key')->nullable();
            $table->text('access_key')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');           
            $table->rememberToken();
            $table->timestamps();

         //   $table->foreign('user_type')->references('id')->on('user_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
