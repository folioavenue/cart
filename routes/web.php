<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::resource('/user','UserController');

Route::post('/cart/check-out-with-paypal','CartController@pay');

Route::get('/order/processed','CartController@success');
Route::get('/paypal/payment','CartController@paymentStatus');
Route::post('/cart/delete-item','CartController@delete_item');
Route::resource('/cart','CartController');
