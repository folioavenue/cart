<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authors = User::where('user_type',2)->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.pages.user.index')
                ->with('authors',$this->authors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.pages.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           
        if($request->cpassword == $request->password ){
             
                $user = User::create([
                    'user_type' => 2,
                    'username'  => $request->username,
                    'name'      => $request->name,
                    'email'     => $request->email,
                    'website'   => $request->website,
                    'api_client_id'=> $request->api_client_id,
                    'api_secret_key'=>$request->api_secret_key,
                    'access_key'=>bcrypt($request->email),
                    'password'  =>bcrypt($request->password) 
                ]);

         }else{

            session()->flash('error_message','Password and Confirm Password did not match!');  

            return redirect()->back();
         }      

             session()->flash('message','Author successfully added!');  

            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
