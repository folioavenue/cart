<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payee;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use Illuminate\Http\Request;

class CartController extends Controller
{
    private $_api_context;

    public function __construct(){
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.pages.form');                                      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Session::flush();
        $flag = false;
        $users   =  User::all();
       
        foreach($users as $user){
            
            if(Hash::check($request->email,$user->access_key)){
                session()->put('client_id',$user->api_client_id);
                session()->put('secret_key',$user->api_secret_key);
                $flag = true;
                break;
            }
        }

        if($flag){
                $book_id    = $request->book_id;
                $book_title = $request->book_title;
                $book_price = $request->book_price;
                $email      = $request->email;
                $book_store = $request->book_store;
                $link       = $request->link;
                $shipping   = 3.80;
                $handling   = 1.99;
                $price = 0;
                $quantity = 0;
                $total_shipping = 0;
                $keys =  []; 
            // $data_session = session()->get('item.book');
                
                $item = [
                        'book_id'    => $book_id,  
                        'book_title' => $book_title,
                        'book_price' => $book_price,
                        'book_store' => $book_store,  
                        'link'       => $link,  
                        'email'      => $email,
                        'handling'   => $handling,
                        'shipping'   => $shipping  
                        ];          

            //  $new_item = array_push($data_session,$item);   
            // session()->start();
            $keys  = session()->get('item.keys');
            
                    // dd(session()->get('item.book'));
            if($keys != null){            
                    if(!in_array($book_id,$keys)){
                    
                        $request->session()->push('item.book'  , $item);               
            
                        $request->session()->push('item.keys'   ,$book_id);  
                    }
                }else{
                        $request->session()->push('item.book'   ,$item);           
            
                        $request->session()->push('item.keys'  ,$book_id);  
                }
                        
                    // $request->session()->push('item.book'  , ['book_title' => $book_title]);
                    // $request->session()->push('item.book'  , ['book_price' => $book_price]);
                    // $request->session()->push('item.book'  , ['secret_key' => $secret_key]);
                    // $request->session()->push('item.book'  , ['handling' => $handling]);
                    // $request->session()->push('item.book'  , ['shipping' => $shipping]);        
                

            // dd(session()->get('item.book'));   
            
            
            $items = collect(session()->get('item.book'));     
            
            foreach($items->unique('book_title') as $item){
                    $quantity += 1;
                    $price  += $item['book_price'];  
            }
            
            if($quantity == 1 ){
                    $total_shipping = $shipping;
                }else{
                    $total_shipping = $shipping + (($quantity - 1)*.55);
                }
            //session()->flush();
                return view('layouts.pages.cart')
                        ->with('items',$items)
                        ->with('total_shipping',$total_shipping)
                        ->with('book_store',$book_store)
                        ->with('link',$link)
                        ->with('total_price',$price)
                        ->with('handling',$handling)
                        ->with('email',$email);
        }else{
            abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $iduj
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_item(Request $request)
    {
        if($request->new_quantity == 0){
            $items = Session::get('item.book');     
            $key = Session::get('item.keys');   
            $client_id = Session::get('client_id');
            $secret_key = Session::get('secret_key');
            unset($items[$request->id]);
            unset($key[$request->id]);
            Session::flush();
            Session::put('item.book',$items);
            Session::put('item.keys',$key);
            Session::put('client_id',$client_id);
            Session::put('secret_key',$secret_key);
        }else{
            Session::flush();
        }
        return $key;
    }   

    public function pay(Request $request)
    {           
            $paypal_conf = \Config::get('paypal');
            $this->_api_context = new ApiContext(new OAuthTokenCredential(Session::get('client_id'), Session::get('secret_key')));
            $this->_api_context->setConfig($paypal_conf['settings']);

            $data_token = $request->_token;
            $toPay = $request->book_price;
            $subtotal = [];
            $items = [];
            // Session::put('pay_amount', $event_category->price);
            // Session::put('data_token', $data_token);

            $payer = new Payer();

            $payer->setPaymentMethod('paypal');
            
            for($i = 0;$i <= count($request->quantity)-1; $i++){
                $item = new Item();
                $item->setName($request->book_title[$i])
                    ->setCurrency('USD')                
                    ->setQuantity($request->quantity[$i])
                    ->setPrice($request->book_price[$i]);

                $subtotal[] = $request->quantity[$i] * $request->book_price[$i];
                $items[] = $item;
            }

            $item_list = new ItemList();
            $item_list->setItems($items);

            
            if(array_sum($request->quantity) == 1 ){
                $total_shipping = $request->shipping;
            }else{
                $total_shipping = $request->shipping + ((array_sum($request->quantity) - 1)*.55);
            }
            
            $details = new Details();            
            $details->setShipping($total_shipping)               
                ->setHandlingFee($request->handling)
                ->setSubtotal(array_sum($subtotal)); 
            $amount = new Amount();
            $amount->setCurrency("USD")
                ->setTotal((array_sum($subtotal))+ $request->handling + $total_shipping)
                ->setDetails($details);
            /* $payee = new Payee();
            $payee->setEmail($user_paypal);*/
            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                // ->setPayee($payee)
                ->setDescription('setDescription')
                ->setInvoiceNumber(uniqid());
            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::to('paypal/payment?success=true'))
                ->setCancelUrl(URL::to('cart'));
            
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            
            try{
                $payment->create($this->_api_context);
                
            }catch(\Paypal\Exception\PPConnectionException $e){
                if(\Config::get('app.debug')){
                    \Session::put('error', 'Connection Timeout');
                    // return Redirect::route('projects');
                    return back();
                }else{
                    \Session::put('error', 'An error occured');
                    // return Redirect::route('projects');
                    return back();
                }
            }
            foreach($payment->getLinks() as $link){
                if($link->getRel() == 'approval_url'){
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            
            Session::put('paypal_payment_id', $payment->getId());
            if(isset($redirect_url)){
                return Redirect::away($redirect_url);
            }
            
            \Session::put('error',' Unknown error occured');
            // return Redirect::route('projects');
            return back();
     
    }

    public function paymentStatus(Request $request) 
    {
        
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(Session::get('client_id'), Session::get('secret_key')));
        $this->_api_context->setConfig($paypal_conf['settings']);
        // $event_category = Category::find($request->get('category'));
        // $event = Event::find($event_category->event->id);
        $paypal_payment_id = $request->get('paymentId');

        $total = Session::get('pay_amount');
        $my_token = Session::get('data_token');

        Session::forget('paypal_payment_id');

        if(empty($request->get('PayerID')) || empty($request->get('token'))){
            \Session::put('error', 'Payment failed');
            // return Redirect::route('projects');
            return back();
        }

        $payment = Payment::get($paypal_payment_id, $this->_api_context);

        $execution = new PaymentExecution();
        
        $execution->setPayerId($request->get('PayerID'));

        try{
        $result = $payment->execute($execution, $this->_api_context);
        }catch(\Paypal\Exception\PPConnectionException $e){
            if(\Config::get('app.debug')){
                session()->flash('error_message', 'Sorry but it seems there is a problem processing your payment!');
                
                return redirect('cart/');
            }else{                
                session()->flash('error_message', 'Sorry but it seems there is a problem processing your payment!');
      
                return redirect('cart/');
            }
        }

        if($result->getState() == 'approved'){
            \Session::flush();
            return redirect('order/processed');
        }

        \Session::put('error','Payment failed');
      
        return back();
    }

   public function success(){
       return view('layouts.pages.user.success_order');
   }
}
